; installer.nsi
;
; Script installer for test application

;--------------------------------

; The name of the installer
Name "installer"

; The file to write
OutFile "TestInstaller_${VERSION}.exe"

; The default installation directory
InstallDir $PROGRAMFILES\Test

RequestExecutionLevel admin

;--------------------------------

; Pages

Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File /r TestApplication\TestApplication\bin\Release\*.*  
  
  WriteUninstaller "uninstall.exe"
  
SectionEnd ; end the section

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Test"
  CreateShortCut "$SMPROGRAMS\Test\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\Test\Test.lnk" "$INSTDIR\TestApplication.exe" "" "$INSTDIR\TestApplication.exe" 0
  
SectionEnd

; Uninstaller

Section "Uninstall"

  ; Remove files and uninstaller
  Delete $INSTDIR\*.*  

  ; Remove directories used
  RMDir "$INSTDIR"

  Delete "$SMPROGRAMS\Test\Uninstall.lnk"
  Delete "$SMPROGRAMS\Test\Test.lnk"
  RMDir  "$SMPROGRAMS\Test"  

SectionEnd
